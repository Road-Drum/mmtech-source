function x = GetHistRGB(ImageFile)
    image = imread(ImageFile);     
%    image = imresize(image,0.25);
    image = double(image);
    
    Red = image(:,:,1); 
    Green = image(:,:,2); 
    Blue = image(:,:,3);   
    
    [Red_i Red_f]=doQuanMatrix(Red, 8 ,0 ,256);
    [Green_i Green_f]=doQuanMatrix(Green, 8 ,0 ,256);
    [Blue_i Blue_f]=doQuanMatrix(Blue, 8 ,0 ,256);

    HistRed=zeros(1,8);
    HistGreen=zeros(1,8);
    HistBlue=zeros(1,8);
    
    for n=1:8
        HistRed(n)=sum(sum(ismember(Red_i,(n-1))));
        HistGreen(n)=sum(sum(ismember(Green_i,(n-1))));
        HistBlue(n)=sum(sum(ismember(Blue_i,(n-1))));
    end
    
%    HistBlue=HistBlue(1:4);
    
    x = [HistRed , HistGreen , HistBlue]; 
    x = x./sum(x);
end