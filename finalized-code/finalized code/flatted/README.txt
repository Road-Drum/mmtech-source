1-Program start, click 'Load image' button. 
2-Open 'Database' menu, click 'Rebuild database' for first time user. Database generation takes at least a few seconds.
3-After database rebuild complete, open 'Database' menu and click 'Load Database'. Choose the newly generated database.
4-After loading 'Database', click 'Search' image to begin search. Retrieved image will be loaded
5-To see most similar images, click 'Show most similar...'. This will show 10 most similar index.
6-'Repair database path' is useful when running the program on different computers. This tool will repair the image path of the database without touching other values.