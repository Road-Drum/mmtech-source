import numpy as np
from skimage.io import imread
from skimage.color import rgb2hsv
from sklearn import preprocessing, model_selection, neighbors
import pandas as pd

imagepath='/home/user/mmtech-source/image.orig/'

#use numpy library
#TODO: test function output

def doquantization(f, L, fMin, fMax):
    Quantize=(fMax-fMin)/L
    QuantIdx=np.floor((f-fMin)/Quantize)
    QuantF=(QuantIdx*Quantize)+(Quantize/2)+fMin
    return QuantIdx, QuantF
    #returns both index and value

def ismembercount(A,B):
    C=np.in1d(A,B)
    return np.count_nonzero(C)
    #we just want member count anyway

def gethistrgb(imagefile):
    image=imread(imagefile)
    #extract each plane   
    red=image[:,:,0]
    green=image[:,:,1]
    blue=image[:,:,2]
    #prep empty vector
    redhist=np.zeros(8)
    greenhist=np.zeros(8)
    bluehist=np.zeros(8)
    #quantization
    red_i,red_f=doquantization(red,8,0,256)
    green_i,green_f=doquantization(green,8,0,256)
    blue_i,blue_f=doquantization(blue,8,0,256)
    for x in range(len(redhist)):
        redhist.flat[x]=ismembercount(red_i,x)
        greenhist.flat[x]=ismembercount(green_i,x)
        bluehist.flat[x]=ismembercount(blue_i,x)
    out = np.concatenate((redhist,greenhist,bluehist))
    return out.reshape(24)/sum(out)
    #WARN: evaluate w/ equivalent MATLAB code
    #value diff is quite significant (tested w/ Octave, probably imagemagick issue)

def gethisthsv(imagefile):
    image=imread(imagefile)
    image=rgb2hsv(image)
    #extract each plane   
    h=image[:,:,0]
    s=image[:,:,1]
    v=image[:,:,2]
    #prep empty vector
    hhist=np.zeros(8)
    shist=np.zeros(8)
    vhist=np.zeros(8)
    #quantization
    h_i,h_f=doquantization(h,8,0.,1.)
    s_i,s_f=doquantization(s,8,0.,1.)
    v_i,v_f=doquantization(v,8,0.,1.)
    for x in range(len(hhist)):
        hhist.flat[x]=ismembercount(h_i,x)
        shist.flat[x]=ismembercount(s_i,x)
        vhist.flat[x]=ismembercount(v_i,x)
    out = np.concatenate((hhist,shist,vhist))
    return out.reshape(24)/sum(out)
    #WARN: evaluate w/ equivalent MATLAB code
    #acceptable value diff

def datasetgen(imagepath,dataset='dataset.data'):
    k=range(1000)
    for n in range(len(k)):
        if n < 100:
            label='Africa'
        elif n > 100 and n < 200:
            label='Beach'
        elif n > 200 and n < 300:
            label='Cobblestone'
        elif n > 300 and n < 400:
            label='Double-Decker'
        elif n > 400 and n < 500:
            label='Dinosaur'
        elif n > 500 and n < 600:
            label='Gajah'
        elif n > 600 and n < 700:
            label='Ros'
        elif n > 700 and n < 800:
            label='Kuda'
        elif n > 800 and n < 900:
            label='Gunung'
        elif n > 900:
            label='Makanan'
        rgb_hist=gethistrgb(imagepath+str(n)+'.jpg')
        hsv_hist=gethisthsv(imagepath+str(n)+'.jpg')
        index_rgb = ['rgb_hist'+str(x) for x in range(len(rgb_hist))]
        index_hsv = ['hsv_hist'+str(x) for x in range(len(hsv_hist))]
        df2=pd.DataFrame({"value": rgb_hist}, index=index_rgb)  #tsame column value as a
        df3=pd.DataFrame({"value": hsv_hist}, index=index_hsv)  #workaround during transpose concatenate
        df4=pd.DataFrame({"label": [label]}, index=['value'])
        #trick to join on index 'value'
        dfjoin=[df2.T, df3.T, df4]
        dfout=pd.concat(dfjoin, axis=1)
        if n == 0:
            df=dfout
        else:
            df=df.append(dfout)
        percentage=(n/999)*100
        print('\rPercentage: %.2f%%' % (percentage), end='\r')
        if percentage == 100.0:
            print('Dataset generated as '+dataset)
    df.to_csv(dataset)
    return df

def setcsv(csvname): 
    df=pd.read_csv(csvname, index_col=False) #don't use broken index
    df=df.drop(['Unnamed: 0'],1)                #drop unused column
    return df

def main(testimg,csvname='dataset.data'): #use 'dataset.data' by default
    df=setcsv(csvname)
    pd.set_option('precision',24)
    #df=df.drop(['hsv_hist'+str(n) for n in range(24)],1) #drop this to test rgb only
    x= np.array(df.drop(['label'],1))
    y= np.array(df['label'])

    x_train, x_test, y_train, y_test = model_selection.train_test_split(x,y) #use default test size 0.25
    print(x_train)
    clf = neighbors.KNeighborsClassifier()
    clf.fit(x_train,y_train)

    accuracy = clf.score(x_test,y_test)
    print(accuracy)

    #recreate dataframe for test subject
    trgb=gethistrgb(imagepath+testimg)
    thsv=gethisthsv(imagepath+testimg)
    index_rgb = ['rgb_hist'+str(x) for x in range(len(trgb))]
    index_hsv = ['hsv_hist'+str(x) for x in range(len(thsv))]
    dfrgb=pd.DataFrame({"value": trgb}, index=index_rgb)  #tsame column value as a
    dfhsv=pd.DataFrame({"value": thsv}, index=index_hsv)  #workaround during transpose 
    dftest=pd.concat([dfrgb.T, dfhsv.T], axis=1)

    predict=clf.predict(dftest)
    print(predict)