function [Q_i , Q_f  ] = doQuanMatrix(f , L , fmin,fmax)  
  Q=(fmax-fmin)/L;
  Q_i=floor((f-fmin)/Q);
  Q_f=Q_i*Q+Q/2+fmin;
end