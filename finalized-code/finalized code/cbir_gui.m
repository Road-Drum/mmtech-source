function varargout = cbir_gui(varargin)
% CBIR_GUI MATLAB code for cbir_gui.fig
%      CBIR_GUI, by itself, creates a new CBIR_GUI or raises the existing
%      singleton*.
%
%      H = CBIR_GUI returns the handle to a new CBIR_GUI or the handle to
%      the existing singleton*.
%
%      CBIR_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CBIR_GUI.M with the given input arguments.
%
%      CBIR_GUI('Property','Value',...) creates a new CBIR_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cbir_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cbir_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cbir_gui

% Last Modified by GUIDE v2.5 21-Sep-2017 07:34:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cbir_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @cbir_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cbir_gui is made visible.
function cbir_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cbir_gui (see VARARGIN)

%initialize many empty handles
handles.img_query = 0;
handles.dbFile = 0;
handles.precision = zeros(1,10);
handles.status = 'Database not loaded!'; % status for each actions done. Current value is for program initialization
set(handles.textState,'String',handles.status); % set status text
% Choose default command line output for cbir_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cbir_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cbir_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% load query image using UI file selector and display the image. handles are used to share data
% between functions

[DBFile,DBPath]=uigetfile('*.jpg','Select image file to load'); 
if DBFile ~= 0   % handle image loading
    handles.img_query=[DBPath DBFile];
    axes(handles.axes1);
    imshow(handles.img_query);
    title('Query image');
    handles.status = ['Image ',DBFile,' was loaded.']; % update status text. Refer OpeningFcn in cbir_gui.m
    set(handles.textState,'String',handles.status);
elseif DBFile == 0  % prevent imshow() from printing errors
    handles.status=['Image loading cancelled.'];       % update status text. Refer OpeningFcn in cbir_gui.m
    set(handles.textState,'String',handles.status);
end
guidata(hObject,handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

QueryImgHSV=GetHistHSV(handles.img_query); %Get HSV feature vector
QueryImgRGB=GetHistRGB(handles.img_query); %Get RGB feature vector

for n=1:length(handles.dbFile.database)
  DatabaseHSV=handles.dbFile.database(n).HistHSV;
  DatabaseRGB=handles.dbFile.database(n).HistRGB;
  handles.dbFile.database(n).EuclidHSV = sqrt(sum((DatabaseHSV-QueryImgHSV).^2)); % Get HSV Euclidian distance
  handles.dbFile.database(n).EuclidRGB = sqrt(sum((DatabaseRGB-QueryImgRGB).^2)); % Get RGB Euclidian distance
end

handles.Result=sort([handles.dbFile.database.EuclidRGB]); % perform sorting with HSV values.

for n=1:length(handles.dbFile.database)
  if handles.dbFile.database(n).EuclidRGB == min(handles.Result) % Search using HSV values
    axes(handles.axes2);
    imshow(handles.dbFile.database(n).ImageFile); % display the retrieved image
    title('Retrieved image');
  end
end
handles.status=['Image search complete.'];       % update status text. Refer OpeningFcn in cbir_gui.m
set(handles.textState,'String',handles.status);
guidata(hObject,handles);

% --------------------------------------------------------------------
function Menu_DB_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_DB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Menu_LoadDB_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_LoadDB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% load database file using UI file selector.

[DBFile,DBPath]=uigetfile('*.mat','Select database file to load');
if DBFile ~= 0 % load database only if the database file has been specified
    handles.dbFile=load([DBPath DBFile]);
    handles.status=['Database ',DBFile,' was loaded from ',DBPath]; % update status text. Refer OpeningFcn in cbir_gui.m
    set(handles.textState,'String',handles.status);
    
elseif DBFile ~= 0 % prevent load() from errors
    handles.status=['Database loading cancelled.'];                 % update status text. Refer OpeningFcn in cbir_gui.m
    set(handles.textState,'String',handles.status);
end
guidata(hObject,handles);

% --------------------------------------------------------------------
function Menu_RebuildDB_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_RebuildDB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

GenerateDB % generate database using the function file

% --------------------------------------------------------------------
function Menu_RepairDB_Callback(hObject, eventdata, handles)
% hObject    handle to Menu_RepairDB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

RepairDBPath % repair database image path using the function file


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% generate most similar images

id=zeros(1,10);  % this will store the sorted file numbers
dbHandler=handles.dbFile.database;
h = waitbar(0,'Generating most similar images. Please wait...') % wait bar as it might be time consuming depending on CPU
for index=1:10
  for n=1:1000
     if dbHandler(n).EuclidRGB == handles.Result(index)  % find the file number from the sorted results
         id(index)=n;
     end
  end
end
  waitbar(n/1000); % wait bar increases for each pass
close(h) % automatically close wait bar window

window = figure('Name','Most similar image','NumberTitle','off'); % generate a window to show the results
set(window, 'MenuBar', 'none'); % disable menubar for better viewing
set(window, 'ToolBar', 'none'); % disable toolbar for better viewing

for n=1:10  % plot 10 most similar images
    idx=id(n);
    imfile = handles.dbFile.database(idx).ImageFile ;
    label = handles.dbFile.database(idx).label;
    strlabel = [sprintf('Label = '),sprintf('%d ',label)];
    strdsim = [sprintf('Dissimilarity = '),sprintf('%f ',dbHandler(idx).EuclidRGB)];
    subplot(4,3,n) , imshow(imfile) , title({strlabel;strdsim});  % show the image with labels and dissimilarity values
end
handles.status=['Generated most similar images.']; % update status text. Refer OpeningFcn in cbir_gui.m
set(handles.textState,'String',handles.status);
guidata(hObject,handles);
