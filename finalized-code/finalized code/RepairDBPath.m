%generate warning message for users to be aware
repairwarn = warndlg(sprintf('Consider backing up the current database before proceeding!\n\nClick OK to continue'),'Repair Database Path');
uiwait(repairwarn);

[DBFile,DBPath]=uigetfile('*.mat','Select database file for path repairing');

if DBFile ~= 0 % perform repair only if database file has been chosen
    load([DBPath DBFile]);
    dbsize=length(database);
    dirimg=uigetdir('','Select the correct directory for the database');
    
    if dirimg ~= 0 % begin repairing only if correct image directory has been supplied
        for n=1:dbsize
            filename=sprintf('%d.jpg',(n-1));
            database(n).imageName=[dirimg filename];
        end

        save('database_cbir.mat','database');
        handles.status = ['Database path has been repaired.'];
        set(handles.textState,'String',handles.status);
        guidata(hObject,handles);
    elseif dirimg == 0 % handle empty image directory to prevent corrupted ImageFile 
        handles.status = ['Database path repair has been cancelled.']; % only notify user using status text. Refer OpeningFcn in cbir_gui.m
        set(handles.textState,'String',handles.status);
        guidata(hObject,handles);
    end
elseif DBFile == 0 % handle empty database filename. Prevent load() from error messages and further corruptions
    handles.status = ['Database path repair has been cancelled.']; % only notify user using status text. Refer OpeningFcn in cbir_gui.m
    set(handles.textState,'String',handles.status);
    guidata(hObject,handles);
end