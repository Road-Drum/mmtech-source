function x = GetHistHSV(ImageFile)
    image = imread(ImageFile);
    image = rgb2hsv(image);
%    image = imresize(image,0.25);
%    image = double(image);
    
    Hue = image(:,:,1); 
    Sat = image(:,:,2); 
    Val = image(:,:,3); 
    
    
    [Hue_i Hue_f]=doQuanMatrix(Hue, 8 ,0 ,1);
    [Sat_i Sat_f]=doQuanMatrix(Sat, 8 ,0 ,1);
    [Val_i Val_f]=doQuanMatrix(Val, 8 ,0 ,1);

    HistHue=zeros(1,8);
    HistSat=zeros(1,8);
    HistVal=zeros(1,8);
    
    for n=1:8
        HistHue(n)= sum(sum(ismember(Hue_i,(n-1))));
        HistSat(n)= sum(sum(ismember(Sat_i,(n-1))));
        HistVal(n)= sum(sum(ismember(Val_i,(n-1))));
    end
    
%    HistVal=HistVal(1:4);
  
    x = [HistHue , HistSat , HistVal];
    x = x./sum(x);
end