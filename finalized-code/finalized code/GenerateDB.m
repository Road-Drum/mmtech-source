%Generate the database_cbir.mat file
imgdir = uigetdir('','Select the image folder'); %User specify image folder

if imgdir ~= 0  % generate the database only if the image folder has been specified
 h = waitbar(0,'Generating database. Please wait...') % interactive loading bar
 for category=1:10
  base=category;
  for samples=((base*100)-99):(base*100)
    imgdir=[imgdir '\'];
    FileName=[imgdir sprintf('%d.jpg', (samples-1))];
    database(samples).ImageFile = FileName;
    database(samples).HistRGB = GetHistRGB(FileName);
    database(samples).HistHSV = GetHistHSV(FileName);
    database(samples).label=base;
    waitbar(samples/1000); % loading bar is filled as a sample is done being processed
  end
 end
 close(h) % automatically close the loading bar
    
% save the variable database
save('database_cbir.mat', 'database');

success = msgbox('Database has been successfully generated', 'Success','help'); % notify with message box
uiwait(success);                                                                % halt until the message box is closed
handles.status = ['Database has been successfully generated.'];                 % update status text. Refer OpeningFcn in cbir_gui.m
set(handles.textState,'String',handles.status);

elseif imgdir == 0 % handle empty value to prevent errors
    %generate warning message and update status text
    generatewarn = warndlg(sprintf('No directory was specified!\n\nDatabase generation has been cancelled.'),'Rebuild Cancelled'); 
    uiwait(generatewarn);                                                                                                          
    handles.status = ['Database creation failed!'];                                                                                
    set(handles.textState,'String',handles.status);
end
guidata(hObject,handles); %update handle structure
clear all; %free up memory